# React Snowpack

> ✨ Bootstrapped with Create Snowpack App (CSA).

## Available Scripts

```bash
$ yarn start # Start server at http://localhost:8080
$ yarn build
$ yarn test

```
