const dependencies = {
  save:
    '@reduxjs/toolkit fontfaceobserver i18next i18next-browser-languagedetector react react-app-polyfill' +
    'react-dom react-helmet-async react-i18next react-redux react-router-dom redux-injectors redux-saga' +
    'sanitize.css styled-components',

  dev:
    '@snowpack/app-scripts-react @snowpack/plugin-dotenv @snowpack/plugin-react-refresh @snowpack/plugin-typescript' +
    '@snowpack/web-test-runner-plugin @types/react @types/react-dom @types/snowpack-env' +
    '@babel/preset-react @babel/preset-typescript' +
    '@snowpack/plugin-babel @snowpack/plugin-postcss @snowpack/plugin-sass' +
    '@web/test-runner prettier snowpack typescript' +
    '@commitlint/cli @commitlint/config-conventional husky' +
    '@types/fontfaceobserver' +
    '@testing-library/react @testing-library/jest-dom @types/jest @types/react-test-renderer @types/testing-library__jest-dom supertest' +
    '@types/react-redux @types/react-router-dom react-scripts' +
    '@types/rimraf rimraf @types/styled-components' +
    'eslint-config-prettier eslint-plugin-prettier eslint-plugin-react-hooks' +
    'cross-env lint-staged ts-jest ts-node @types/node i18next-scanner',
};

module.exports = dependencies;
